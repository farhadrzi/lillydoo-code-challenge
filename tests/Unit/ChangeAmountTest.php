<?php


namespace Unit;


use App\Machine\Firmware\WorkingPrototypeFirmware;
use App\Machine\SnackMachine;
use App\Product\ProductInterface;
use PHPUnit\Framework\TestCase;

class ChangeAmountTest extends TestCase
{
    private array $slotOfProduct;
    private SnackMachine $snackMachine;
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $firmware = new WorkingPrototypeFirmware();
        $mockProductInterface = $this->createMock(ProductInterface::class);
        $this->snackMachine = new SnackMachine($firmware, $mockProductInterface);
        $this->slotOfProduct = $this->snackMachine->loadMachine();
    }

    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        try {
            $reflection = new \ReflectionClass(get_class($object));
            $method = $reflection->getMethod($methodName);
            $method->setAccessible(true);
        } catch (\ReflectionException $e) {
        }
        return $method->invokeArgs($object, $parameters);
    }

    public function testSerializeArray(){
        $testArray=[1,1,0.5,0.02];
        $expectArray=[["number"=>1,'count'=>2],["number"=>0.5,'count'=>1],["number"=>0.02,'count'=>1]];
        $resultArray = $this->invokeMethod($this->snackMachine, 'serializeChangeList',array($testArray));
        $this->assertEquals($expectArray, $resultArray);
    }

    public function testHaveValidChange(){
        $allAvailableCoin = array(0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1.00, 2.00);

        $result = $this->invokeMethod($this->snackMachine, 'haveValidChange',array($allAvailableCoin,7,2.5,10));
        $this->assertEquals(true, $result);
    }

    public function testAddChangeToArray(){
        $testArray=[["number"=>1,'count'=>2]];
        $resultArray = $this->snackMachine->addCountToNumber($testArray, 1);
        $this->assertEquals(3, $resultArray[0]['count']);
    }
}