<?php


namespace Unit;


use App\Machine\Firmware\WorkingPrototypeFirmware;
use App\Machine\MachineInterface;
use App\Machine\SnackMachine;
use App\Product\ProductInterface;
use phpDocumentor\Reflection\Types\Self_;
use PHPUnit\Framework\TestCase;

class ShowStackTest extends TestCase
{
    private array $slotOfProduct;
    private SnackMachine $snackMachine;
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $firmware = new WorkingPrototypeFirmware();
        $mockProductInterface = $this->createMock(ProductInterface::class);
        $this->snackMachine = new SnackMachine($firmware, $mockProductInterface);
        $this->slotOfProduct = $this->snackMachine->loadMachine();
    }

    public function testLoadProduct(){

        $this->assertEquals('Mars',  $this->slotOfProduct['1a']->getName());
    }

    public function testGetProductWithSlotId(){
        $product = $this->snackMachine->getProductWithSlotId('1a');
        $this->assertNotEmpty($product);
    }
}