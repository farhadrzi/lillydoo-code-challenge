<?php


namespace Functional;


use App\Machine\Firmware\WorkingPrototypeFirmware;
use App\Machine\Purchase\Transaction;
use App\Machine\SnackMachine;
use App\Product\Product;
use App\Product\ProductInterface;
use PHPUnit\Framework\TestCase;

class ExecuteTransactionTest extends TestCase
{
    private array $slotOfProduct;
    private SnackMachine $snackMachine;
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $firmware = new WorkingPrototypeFirmware();
        $mockProductInterface = new Product();
        $this->snackMachine = new SnackMachine($firmware, $mockProductInterface);
        $this->slotOfProduct = $this->snackMachine->loadMachine();
    }

    public function testExecute(){
        $purchaseTransaction = new Transaction();
        $purchaseTransaction = $purchaseTransaction->createTransactionInterFace('1a', 10.0, 2);
        $resultArray = $this->snackMachine->execute($purchaseTransaction);
        $this->assertArrayHasKey('number', $resultArray[0]);
    }
}