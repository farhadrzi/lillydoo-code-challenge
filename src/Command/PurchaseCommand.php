<?php

declare(strict_types=1);

namespace App\Command;

use App\Machine\MachineInterface;
use App\Machine\Purchase\TransactionInterface;
use App\Machine\SnackMachine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class PurchaseCommand extends Command
{

    /**
     * @var MachineInterface
     */
    private MachineInterface $machineInterface;
    /**
     * @var TransactionInterface
     */
    private TransactionInterface $transactionInterface;

    public function __construct(MachineInterface $machineInterface,TransactionInterface $transactionInterface)
    {
        $this->machineInterface = $machineInterface;
        parent::__construct();
        $this->transactionInterface = $transactionInterface;
    }

    protected  function configure(): void
    {
        $this
            ->setName('purchase')
            ->addArgument(
                'product',
                InputArgument::OPTIONAL
            )
            ->addArgument(
                'quantity',
                InputArgument::OPTIONAL
            )->addArgument(
                'amount',
                InputArgument::OPTIONAL
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // $snackMachine = new SnackMachine...

        $this->machineInterface->loadMachine();
        $this->transactionInterface->createTransactionInterFace($input->getArgument('product'), $input->getArgument('amount'), $input->getArgument('quantity'));
        $product = $this->machineInterface->getProductWithSlotId($input->getArgument('product'));
        $finalList = $this->machineInterface->execute($this->transactionInterface);
        $productName = $product->getName();
        $productPrice = $product->getPrice();
        $quantity = $input->getArgument('quantity');
        $totalPrice = $productPrice * $quantity;

        $output->writeLn("You bought ${quantity} packs of ${productName} for ${totalPrice}€, each for ${productPrice}€");
        $output->writeLn("Your change is: ");
        $rows=array();
        foreach ($finalList as $changes){
            $rows[]=[$changes['number'],$changes['count']];
        }
        $table = new Table($output);
        $table
            ->setHeaders(['Coins', 'Count'])
            ->setRows($rows);

        $table->render();

        return Command::SUCCESS;
    }
}
