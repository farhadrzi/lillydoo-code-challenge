<?php

declare(strict_types=1);

namespace App\Command;

use App\Machine\MachineInterface;
use App\Machine\SnackMachine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class ShowStockCommand extends Command
{
    #AsCommand([name: 'show-stock',description: 'Show Machine Stocks', hidden: false,aliases: ['show-stock']])


    /**
     * @var MachineInterface
     */
    private MachineInterface $machineInterface;

    public function __construct(MachineInterface $machineInterface)
    {
        $this->machineInterface = $machineInterface;
        parent::__construct();
    }

    protected  function configure(): void
    {

        $this
            ->setName('show-stack')
            ->setDescription('Show Machine Stack')
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $slotOfProduct = $this->machineInterface->loadMachine();

        $header=array('','a','b');
        $row= array('1','2');

        $table = new Table($output);
        $table->setHeaders($header);
        for($i=1;$i<sizeof($header);$i++){
            $data=array($row[$i-1]);
            foreach ($row as $rowValue){
                $data[]=$slotOfProduct[$rowValue.$header[$i]]->getName();
            }
            $table->addRow($data);
        }

        $table->render();

        return Command::SUCCESS;
    }
}
