<?php

declare(strict_types=1);

namespace App\Machine\Firmware;


class WorkingPrototypeFirmware implements FirmwareInterface
{
    /**
     * @return string[][]
     */
    public function getSlots(): array
    {
        return array(['1a','2a'],['1b','2b']);
    }
}
