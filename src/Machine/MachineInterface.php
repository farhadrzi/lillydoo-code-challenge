<?php

declare(strict_types=1);

namespace App\Machine;

use App\Machine\Purchase\TransactionInterface as PurchaseTransactionInterface;
use App\Product\Product;

interface MachineInterface
{
    public function execute(PurchaseTransactionInterface $purchaseTransaction);
    public function loadMachine():array;
    public function getProductWithSlotId(string $slotId):Product;
}
