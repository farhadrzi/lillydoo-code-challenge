<?php

declare(strict_types=1);

namespace App\Machine;

use App\Machine\Firmware\FirmwareInterface;
use App\Machine\Purchase\TransactionInterface as PurchaseTransactionInterface;
use App\Product\Product;
use App\Product\ProductInterface;

class SnackMachine implements MachineInterface
{
    /**
     * @var FirmwareInterface
     */
    private FirmwareInterface $firmware;
    private array $slotOfProduct;
    private array $allProduct;
    /**
     * @var ProductInterface
     */
    private ProductInterface $productInterface;

    public function __construct(FirmwareInterface $firmware,ProductInterface $productInterface)
    {

        $this->firmware = $firmware;
        $this->slotOfProduct=array();
        $this->productInterface = $productInterface;
    }

    /**
     * Create Product Object For Machine
     */
    private function createAllProduct(){

        $this->allProduct[]=(new Product("Mars",3.69))->createProduct();
        $this->allProduct[]=(new Product("Coca",1.77))->createProduct();
        $this->allProduct[]=(new Product("Pepsi",2.26))->createProduct();
        $this->allProduct[]=(new Product("M&M's",2.99))->createProduct();
    }

    /**
     * Load Machine And Load Slots
     * @return array
     */
    public function loadMachine():array
    {
        $this->createAllProduct();
        $counter=0;
        for ($i=0;$i<sizeof($this->firmware->getSlots());$i++){
            for ($j=0;$j<sizeof($this->firmware->getSlots()[$i]);$j++){
                $product = $this->allProduct[$counter];
                $counter=$counter+1;
                $this->slotOfProduct[$this->firmware->getSlots()[$i][$j]]=$product;
            }
        }
        return $this->slotOfProduct;
    }

    /**
     * @param string $slotId
     * @return Product
     */
    public function getProductWithSlotId(string $slotId):Product{
        return $this->slotOfProduct[$slotId];
    }

    /**
     *
     * Run Purchase Functions
     * @param PurchaseTransactionInterface $purchaseTransaction
     * @return array|void
     */
    public function execute(PurchaseTransactionInterface $purchaseTransaction)
    {

        $allAvailableCoin = $purchaseTransaction->sortChangesCoin();
        $product = $this->slotOfProduct[$purchaseTransaction->getSlotId()];
        $totalPrice = $product->getPrice()*$purchaseTransaction->getQuantity();

        if($purchaseTransaction->getPaidAmount()<$totalPrice){
            error_log('insufficient balance');
            return;
        }

        $remainBalance = floatval($purchaseTransaction->getPaidAmount()-$totalPrice);
        $cheapestPrice = floatval($this->productInterface->calculateCheapestProduct($this->allProduct));
        $coinIndex =0;
        $changesList = array();
        while ($remainBalance>0){
            $remainBalance= round($remainBalance,2);
            if($this->haveValidChange($allAvailableCoin,$coinIndex,$cheapestPrice,$remainBalance)){
                $changesList[]=$allAvailableCoin[$coinIndex];
                $remainBalance =round($remainBalance-$allAvailableCoin[$coinIndex],2);
            }else{
                $coinIndex++;
            }
        }

        return $this->serializeChangeList($changesList);
    }

    /**
     *
     * Serialize For Array
     * @param array $changeList
     * @return array
     */
    private function serializeChangeList(array $changeList):array{
        $finalArray = array();
        for ($i=0;$i<sizeof($changeList);$i++){
            $change = $changeList[$i];
            $finalArray = $this->addCountToNumber($finalArray, $change);
        }
        return $finalArray;
    }

    /**
     * @param $allAvailableCoin
     * @param $coinIndex
     * @param $cheapestPrice
     * @param $remainBalance
     * @return bool
     */
    private function haveValidChange($allAvailableCoin,$coinIndex,$cheapestPrice,$remainBalance):bool{
        if($allAvailableCoin[$coinIndex]<$cheapestPrice) {
            if (round($remainBalance - $allAvailableCoin[$coinIndex], 2) >= 0.000) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * @param $array
     * @param $number
     * @return mixed
     */
    public function addCountToNumber($array,$number){
        $numberFounded = false;
        foreach ($array as &$change){
            if($change['number']==$number){
                $numberFounded=true;
                $change['count'] = $change['count']+1;
            }
        }
        if(!$numberFounded){
            $array[]=['number'=>$number,'count'=>1];
        }
        return $array;
    }
}
