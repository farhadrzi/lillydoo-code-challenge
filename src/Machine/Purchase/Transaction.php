<?php


namespace App\Machine\Purchase;


class Transaction implements TransactionInterface
{
    private float $paidAmount;
    private int $quantity;
    private string $slotId;
    private array $allAvailableCoin = array(0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1.00, 2.00);

    /**
     * @param $slotId
     * @param $paidAmount
     * @param $quantity
     * @return TransactionInterface
     */
    public function createTransactionInterFace($slotId, $paidAmount, $quantity): TransactionInterface
    {
        $this->paidAmount = $paidAmount;
        $this->quantity = $quantity;
        $this->slotId = $slotId;
        return $this;
    }

    /**
     * @return float
     */
    public function getPaidAmount(): float
    {
        return $this->paidAmount;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getSlotId(): string
    {
        return $this->slotId;
    }

    /**
     * @return array
     */
    public function sortChangesCoin():array{
        $array = $this->allAvailableCoin;
        for($i=1;$i<sizeof($array);$i++){
            $current = $array[$i];
            $j = $i-1;
            while($j>=0 && $array[$j]<$current){
                $array[$j+1] = $array[$j];
                $j--;
            }
            $array[$j+1] = $current;
        }

        return $array;
    }
}