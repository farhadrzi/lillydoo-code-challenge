<?php

declare(strict_types=1);

namespace App\Machine\Purchase;

interface TransactionInterface
{
    public function createTransactionInterFace($slotId,$paidAmount,$quantity):TransactionInterface;
    public function getPaidAmount(): float;
    public function getQuantity(): int;
    public function getSlotId(): string;
    public function sortChangesCoin(): array;
}
