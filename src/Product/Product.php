<?php


namespace App\Product;


class Product implements ProductInterface
{
    public string $productName;
    public string $productPrice;

    /**
     * Product constructor.
     * @param string $name
     * @param float $price
     */
    public function __construct(string $name='',float $price=0.0)
    {
        $this->productName =$name;
        $this->productPrice =$price;
    }

    /**
     * @return ProductInterface
     */
    public function createProduct():ProductInterface
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->productName;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->productPrice;
    }

    /**
     * @param array $allProduct
     * @return float
     */
    public function calculateCheapestProduct(array $allProduct):float{
        $cheapestPrice = 100000.0;
        foreach ($allProduct as $product){
            if($product->getPrice()<$cheapestPrice){
                $cheapestPrice = $product->getPrice();
            }
        }
        return $cheapestPrice;
    }

}