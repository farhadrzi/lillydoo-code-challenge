<?php


namespace App\Product;


interface ProductInterface
{
    public function createProduct():ProductInterface;
    public function getName(): string;
    public function getPrice(): float;
    public function calculateCheapestProduct(array $allProduct): float;

}
